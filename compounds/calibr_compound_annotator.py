import pandas as pd
import sys
import os
import requests
import simplejson
import PBB_Core
import pprint
import numpy as np

import cdk_pywrapper

from chemlib import PubChemMolecule, InChIKeyMissingError, ChemSpiderMolecule, ChEMBLMolecule, GTPLMolecule, \
    UNIIMolecule

# data aggregator for Calibr compounds

__author__ = 'Sebastian Burgstaller-Muehlbacher'
__license__ = 'AGPLv3'
__copyright__ = 'Sebastian Burgstaller-Muehlbacher'


headers = {
    'accept': 'application/json',
    'content-type': 'application/json',
    'charset': 'utf-8'
}

s = requests.Session()

columns = ['inchi_key', 'inchi', 'orig_smiles', 'csmiles', 'ismiles', 'cid', 'sids', 'chem_names', 'unii',
           'trivial_name', 'chembl', 'surechembl', 'chebi', 'wikidata', 'chemspider', 'gtpl']

calibr_filename = 'calibr_annotation.csv'


def load_smiles():
    smiles_df = pd.DataFrame(columns=columns)

    if os.path.isfile(calibr_filename):
        with open(calibr_filename, 'r') as infile:
            return pd.read_csv(infile, index_col=0)

    smiles_list = []
    f = open('calibr_compounds', 'rt')
    while f:
        line = f.readline()
        if not line:
            break

        line = line.strip().replace('*', '')
        print(line)
        smiles_list.append(line)

    for count, s in enumerate(smiles_list):
        try:
            cmpnd = cdk_pywrapper.Compound(compound_string=s, identifier_type='smiles')
            isomeric_smiles = cmpnd.get_smiles(smiles_type='isomeric')
            canonical_smiles = cmpnd.get_smiles(smiles_type='generic')
            ikey = cmpnd.get_inchi_key()
            inchi = cmpnd.get_inchi()

            smiles_df.loc[count, 'orig_smiles'] = s
            smiles_df.loc[count, 'inchi_key'] = ikey
            smiles_df.loc[count, 'inchi'] = inchi
            smiles_df.loc[count, 'csmiles'] = canonical_smiles
            smiles_df.loc[count, 'ismiles'] = isomeric_smiles

            print(ikey)
            # print(inchi)

        except ValueError as e:
            print(e)
            print('SMILES conversion failed')

    smiles_df.to_csv(calibr_filename)

    return smiles_df


def get_ikeys():
    """
    retrieves all InChI keys currently in Wikidata
    :return: A dictionary with InChI keys as key and Wikidata QID (full PURL) as value
    """
    query = '''
        select * where {
            ?c wdt:P235 ?ikey .
        }
        '''

    r = PBB_Core.WDItemEngine.execute_sparql_query(query=query)['results']['bindings']

    return {x['ikey']['value']: x['c']['value'] for x in r}


def get_pubchem_cid(df):
    not_found_count = 0
    for count in df.index:
        ikey = df.loc[count, 'inchi_key']
        print(ikey)

        url = 'https://pubchem.ncbi.nlm.nih.gov/rest/rdf/inchikey/{}.json'.format(ikey)

        try:
            r = s.get(url, headers=headers).json()
        except simplejson.JSONDecodeError as e:
            print(e.__str__())
            print('PubChem does not have this InChI key')
            not_found_count += 1
            continue

        cids = list()
        if 'http://semanticscience.org/resource/is-attribute-of' in r['inchikey/{}'.format(ikey)]:
            for x in r['inchikey/{}'.format(ikey)]['http://semanticscience.org/resource/is-attribute-of']:
                cids.append(x['value'].split('/')[-1])

            df.loc[count, 'cid'] = cids[0]
            print(cids)

    print(not_found_count, ' InChI keys have not been found in PubChem')
    df.to_csv('tmp_cid_aggregate.csv')


def print_stats():
    if os.path.isfile(calibr_filename):
        with open(calibr_filename, 'r') as infile:
            df = pd.read_csv(infile, index_col=0)

    for ikey, qid in get_ikeys().items():
        if ikey in list(df['inchi_key'].values):
            df.loc[df['inchi_key'].values == ikey, 'wikidata'] = qid

    print(df.count())


def compare_wd_rep():
    df = pd.DataFrame(columns=columns)
    ikey_prob_count = 0

    if os.path.isfile(calibr_filename):
        with open(calibr_filename, 'r') as infile:
            df = pd.read_csv(infile, index_col=0)

    # add Wikidata QIDs if available
    for ikey, qid in get_ikeys().items():
        if ikey in list(df['inchi_key'].values):
            df.loc[df['inchi_key'].values == ikey, 'wikidata'] = qid

    for count, row in df.iterrows():
        if pd.notnull(row['cid']) or pd.notnull(row['chemspider']):
            continue

        ikey = row['inchi_key']
        print(ikey)
        mol_types = [PubChemMolecule,  ChEMBLMolecule, GTPLMolecule, UNIIMolecule]
        mol_instances = []

        for x in mol_types:
            try:

                mol_instances.append(x(inchi_key=ikey))

            except Exception as e:
                print(e)
                mol_instances.append(None)
                continue

        cs = []
        try:
            cs = ChemSpiderMolecule.search(ikey)
        except Exception as e:
            print(e)

        df.loc[count, 'cid'] = mol_instances[0].cid if mol_instances[0] else np.NaN
        df.loc[count, 'sid'] = '|'.join(mol_instances[0].sids) if mol_instances[0] else np.NaN
        df.loc[count, 'chem_names'] = mol_instances[0].main_label if mol_instances[0] else np.NaN
        df.loc[count, 'assay_id'] = '|'.join(['{}:{}'.format(k, ','.join(v)) for k, v in
                                              mol_instances[0].assay_ids.items()]) if mol_instances[0] else np.NaN

        df.loc[count, 'chemspider'] = '|'.join([x.csid for x in cs if x]) if cs else np.NaN
        df.loc[count, 'trivial_name'] = '|'.join([x.common_name if x.common_name else '' for x in cs]) if cs else np.NaN

        df.loc[count, 'chembl'] = mol_instances[1].chembl_id if mol_instances[1] else np.NaN
        df.loc[count, 'chebi'] = mol_instances[1].chebi if mol_instances[1] else np.NaN

        df.loc[count, 'gtpl'] = mol_instances[2].gtpl_id if mol_instances[2] else np.NaN

        df.loc[count, 'unii'] = mol_instances[3].unii if mol_instances[3] else np.NaN

        if count % 20 == 0:
            df.to_csv(calibr_filename)
            print('Total count is', count)
            pprint.pprint(df.count())

    pprint.pprint(df.count())

    df.to_csv(calibr_filename)


def main():
    load_smiles()
    compare_wd_rep()
    print_stats()

if __name__ == '__main__':
    sys.exit(main())